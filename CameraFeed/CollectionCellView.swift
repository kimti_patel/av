//
//  CollectionCellView.swift
//  CameraFeed
//
//  Created by kim on 1-03-16.
//  Copyright © 2016 SkyeApps. All rights reserved.
//


import UIKit


class CollectionCellView: UICollectionViewCell
{
    
   
    @IBOutlet var textLabel: UILabel?
    @IBOutlet var imageView: UIImageView?

    }
