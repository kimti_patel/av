//
//  CameraSingleton.swift
//  CameraFeed
//
//  Created by kim on 3-03-16.
//  Copyright © 2016 SkyeApps. All rights reserved.
//

import UIKit

class CameraSingleton: UIViewController
{
    var detailImage : UIImage!
    
    static let sharedInstance = CameraSingleton()
}
