//
//  photoViewVC.swift
//  CameraFeed
//
//  Created by kim on 2-03-16.
//  Copyright © 2016 SkyeApps. All rights reserved.
//

import UIKit
import AVFoundation


class photoViewVC: UIViewController
{
    
    @IBOutlet var imageView: UIImageView?
    
    var image : UIImage?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let defaultImage = UIImage(named: "playButton.png", inBundle: NSBundle.mainBundle(), compatibleWithTraitCollection: nil)

        image = CameraSingleton.sharedInstance.detailImage
        
        if ((image) != nil)
        {
            print("Image found")
            imageView?.image = image
            
        }
        else
        {
            print("Image Not Found")
            
            imageView?.image = defaultImage

        }
        
        
    }
    
    @IBAction func goBackToTableView(sender : UIButton)
    {
        self.navigationController?.popViewControllerAnimated(true)?.viewWillAppear(true)
    }


    
}
