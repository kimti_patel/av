//
//  VideoViewController.swift
//  CameraFeed
//
//  Created by kim on 1-03-16.
//  Copyright © 2016 SkyeApps. All rights reserved.
//

import UIKit

import AVKit
import AVFoundation
import MobileCoreServices

import Photos



class VideoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegate
{
    @IBOutlet  var collectionView : UICollectionView?
    
    
    let imagePicker: UIImagePickerController! = UIImagePickerController()

    var counter : Int?
    
    var collectionArray : NSMutableArray?
    
    var cameraAccesible : Bool?
    
    var  VideoImageFile :String = "VideoImageFiles.plist"
    
    var collectionCellImage : UIImage?
    
    // check before loading new instance of Camera to avoide error: Ensure your view has been rendered at least once before snapshotting or snapshot after screen updates.
    var showCamera : Bool?

    override func viewDidLoad()
    {
        super.viewDidLoad()

        counter = NSUserDefaults.standardUserDefaults().objectForKey("counter") as? Int
        
        cameraAccesible = self.checkifCameraAccessible()
        
        self.getCollectionArrayFromFile()
        
        collectionCellImage = self.getImageForCollectionCell()
    }
    
    // MARK: Record a video - "Take Video" Button action
    
    @IBAction func recordVideo(sender: AnyObject)
    {
        print("takeVideo")
        if(cameraAccesible == true && showCamera == true)
        {
            imagePicker.mediaTypes = [kUTTypeMovie as String]
            
            presentViewController(imagePicker, animated: true, completion: {})

        }

    }
    
    // MARK: Take photo - "Take Video" Button action
    @IBAction func takePhoto(sender: AnyObject)
    {
        print("takePhoto")
        if(cameraAccesible == true && showCamera == true)
        {
            imagePicker.mediaTypes = [kUTTypeImage as String]
            
            presentViewController(imagePicker, animated: true, completion: {})

        }
        
    }
    
   func checkifCameraAccessible() -> Bool
   {
    if (UIImagePickerController.isSourceTypeAvailable(.Camera))
    {
        if UIImagePickerController.availableCaptureModesForCameraDevice(.Front) != nil
        {
            
            imagePicker.sourceType = .Camera
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            
            
            showCamera = true
            return true
        }
        else
        {
            popAlert("Front camera doesn't exist", message: "Application cannot access the camera.")
            
            return false
        }
    } else
    {
        popAlert("Camera not accessable", message: "Application cannot access the camera.")
        return false
    }

    }
    
    func saveImageAsAsset(image: UIImage, completion: (localIdentifier:String?) -> Void)
    {
        
        var imageIdentifier: String?
        
        PHPhotoLibrary.sharedPhotoLibrary().performChanges({ () -> Void in
            let changeRequest = PHAssetChangeRequest.creationRequestForAssetFromImage(image)
            let placeHolder = changeRequest.placeholderForCreatedAsset
            imageIdentifier = placeHolder!.localIdentifier
            }, completionHandler: { (success, error) -> Void in
                if success {
                    completion(localIdentifier: imageIdentifier)
                } else {
                    completion(localIdentifier: nil)
                }
        })
    }
   // MARK: UIImagePickerControllerDelegate delegate methods

    
    // Finished recording a video
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        print("Video recorded")
        let type = info[UIImagePickerControllerMediaType];
        
        if( type! .isEqualToString(kUTTypeImage as String) )
        {
            //image
            
            if let capturedImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)
            {
                

                var imageIdentifier: String?

                saveImageAsAsset(capturedImage, completion: { (localIdentifier) -> Void in imageIdentifier = localIdentifier
                })
                
                
                // adding to play list
                let imageData = UIImageJPEGRepresentation(capturedImage, 1)
                
                let fileName = String(format: "test%d.png",counter!)
                
                //let fileName = "/test \(counterString).png"
                
                imageData?.writeToFile(self.getDocFilePath(fileName), atomically: false)
                
                let tempDict =  ["fileName": fileName, "type" : "png"]
                
                self.addToPlistFile(tempDict)
                
                showCamera = true
            }
        }
        else
        {
            //video
            if let videoURL:NSURL = (info[UIImagePickerControllerMediaURL] as? NSURL)
            {
                // Save video to the user's photoAlbum
                let selectorToCall = Selector("videoWasSavedSuccessfully:didFinishSavingWithError:context:")
                
                UISaveVideoAtPathToSavedPhotosAlbum(videoURL.relativePath!, self, selectorToCall, nil)
                
                // Save the video to Doc Directory to play later
                let videoData = NSData(contentsOfURL: videoURL)
                
                // Creating File Name.. Ex test1.mp4, which incresed by one when new video created
                
                let fileName = String(format: "test%d.mp4",counter!)

               // let  fileName = "/test \(counterString).mp4"
                
                videoData?.writeToFile(self.getDocFilePath(fileName), atomically: false)
                
                let tempDict =  ["fileName": fileName, "type" : "mp4"]
                
                self.addToPlistFile(tempDict)
                
            }
        }
        
        
      
        
        imagePicker.dismissViewControllerAnimated(true, completion:{
            self.popAlert("Saved", message: "")
        })
    }
    
    // User canceled recording
    @objc func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        dismissViewControllerAnimated(true, completion: nil)
        
        
        showCamera = true
    }
    
    
    // Task to perform after recording video
    func videoWasSavedSuccessfully(video: String, didFinishSavingWithError error: NSError!, context: UnsafeMutablePointer<()>)
    {
       
        if let theError = error
        {
            print("Error saving video = \(theError)")
            
            showCamera = false
        }
        else
        {
            print("Saved")
            
            
            showCamera = true

        }
    }
    //
    func getImageForCollectionCell() -> UIImage
    {
        var image: UIImage?
        
        
        image = UIImage(named: "playButton.png", inBundle: NSBundle.mainBundle(), compatibleWithTraitCollection: nil)

         return image!
        

    }
    // MARK: Pop Alert
    
    func popAlert(title: String, message: String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK:file Name plist created to load later
    func addToPlistFile(avDictionary: Dictionary<String, String>)
    {
     //   var fileNameArray  = [String :String]()
        
        var fileNameArray = NSMutableArray()
        
        let plistData:NSData!
        
        let fileManager = NSFileManager.defaultManager()
        
        let plistFilePath = self.getDocFilePath(VideoImageFile)
        
        if (fileManager.fileExistsAtPath(plistFilePath))
        {
            print("File Exist At path")
            fileNameArray = NSMutableArray(contentsOfFile: plistFilePath)!
            
        }
       
        fileNameArray.addObject(avDictionary)
        
        collectionArray = fileNameArray
        
        
        print("FileNameArray while saving: \(fileNameArray)")

        //Add new Video fileName to Array
        
        
        collectionView?.reloadData()
        
        if fileNameArray.count > 0
        {
            do{
                
                plistData = try NSPropertyListSerialization.dataWithPropertyList(fileNameArray, format: .XMLFormat_v1_0, options: 0)
                
                plistData?.writeToFile(plistFilePath, atomically: true)
                
                counter = counter! + 1
                NSUserDefaults.standardUserDefaults().setInteger(counter!, forKey: "counter")

                
                
            }catch
            {
                print("Error Serializing Array into NSData")
            }
            
        }
        
    }
    
    
    //MARK : getDocumentFilePath
    func getDocFilePath(fileName :String) -> String
    {
        
        
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        
        let documentsDirectory: AnyObject = paths[0]
        
        let dataPath = documentsDirectory.stringByAppendingPathComponent(fileName)
        
        return dataPath
    }
    
    // MARK :  UICollectionView Delegate protocols
    
    //get Array(Video File Names) to show in collection
    func getCollectionArrayFromFile()
    {
        
        let fileManager = NSFileManager.defaultManager()
        
        let plistFilePath = self.getDocFilePath(VideoImageFile)
        
        if (fileManager.fileExistsAtPath(plistFilePath))
        {
            print("File Exist At path")
            collectionArray = NSMutableArray(contentsOfFile: plistFilePath)!
        }
        
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
        
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        print("collection Array:\(collectionArray)")
        if(collectionArray?.count > 0)
        {
            return (collectionArray?.count)!
            
        }
        return 0
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CollectionViewCell", forIndexPath: indexPath) as? CollectionCellView
        
        
        let tempDic : NSDictionary = collectionArray![indexPath.row] as! NSDictionary
        
        cell!.textLabel?.text = tempDic["fileName"] as? String
        
        let fileType : String = (tempDic["type"] as? String)!
        
        //check filetype and load image accordingly
        if fileType == "mp4"
        {
            cell!.imageView?.image = collectionCellImage
        }
        else
        {
            
            let image : UIImage = self.loadPhoto((tempDic["fileName"] as? String)!)
                
            cell!.imageView?.image = image
        }
        
        return cell!
        
    }
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        print("cell selected")
        let tempDic : NSDictionary = collectionArray![indexPath.row] as! NSDictionary
        
        //check filetype and load detailView accordingly
        let fileType : String = (tempDic["type"] as? String)!


        if fileType == "mp4"
        {
            print("load Video")
            self.playVideo(tempDic["fileName"]  as! String)
        }
        else
        {
            print("load Image")
            let photoVC : photoViewVC = self.storyboard!.instantiateViewControllerWithIdentifier("photoViewVC") as! photoViewVC
            
            let fileImage : UIImage = self.loadPhoto((tempDic["fileName"] as? String)!)
            
            CameraSingleton.sharedInstance.detailImage = fileImage
                
            self.navigationController?.pushViewController(photoVC, animated: true)
        }
    

    }

   // Play the video recorded for the app
    func playVideo(userSelectedPath: String)
    {
        print("Play a video")
        
        // Find the video in the app's document directory
        
        let videoAsset = (AVAsset(URL: NSURL(fileURLWithPath: self.getDocFilePath(userSelectedPath))))
        let playerItem = AVPlayerItem(asset: videoAsset)
        
        // Play the video
        let player = AVPlayer(playerItem: playerItem)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        self.presentViewController(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
        
    }
    
    func loadPhoto(fileName: String) -> UIImage
    {
        let imgPath: String = self.getDocFilePath(fileName)

        let myImage  = UIImage(contentsOfFile: imgPath)
        
       
        return myImage!
    }

}
